﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class CharacterMovementController : MonoBehaviour
{
    public CharacterInfoAsset characterInfoAsset;

    Rigidbody body;

    void Awake()
    {
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");
        float horizontal = CrossPlatformInputManager.GetAxisRaw("Horizontal");

        body.velocity = (transform.right * horizontal + transform.forward * vertical) * characterInfoAsset.movementSpeed;
    }
}
