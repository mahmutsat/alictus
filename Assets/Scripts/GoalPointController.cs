﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalPointController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<IDestroyable>() != null)
        {
            other.GetComponent<IPooledObject>().Destroy();
            GameManager.Instance.TotalCollectedObjectCount++;
        }
    }
}
