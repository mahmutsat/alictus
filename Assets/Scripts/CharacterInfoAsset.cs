﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterInfoAsset", menuName = "Characters/CharacterInfo")]
public class CharacterInfoAsset : ScriptableObject
{
    public int movementSpeed = 10;
}
