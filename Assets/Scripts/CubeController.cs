﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeController : MonoBehaviour, IPooledObject, IDestroyable
{
    [Space]
    [SerializeField]
    string tag;
    public void Destroy()
    {
        ObjectManager.Instance.Destroy(this.gameObject, tag);
    }
}
