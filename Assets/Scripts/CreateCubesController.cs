﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCubesController : MonoBehaviour
{
    [Space]
    [SerializeField]
    Texture2D texture;
    [SerializeField]
    Transform cubeHolder;

    void Start()
    {
        CreateBoxes();
    }

    void CreateBoxes()
    {
        float width = texture.width;
        float height = texture.height;

        for (int cubeXPosition = 0; cubeXPosition < width; cubeXPosition++)
        {
            for (int cubeYPosition = 0; cubeYPosition < height; cubeYPosition++)
            {
                var color = texture.GetPixel(cubeXPosition, cubeYPosition);

                if (color.a < 0.1f)
                    continue;

                var pos = new Vector3(cubeXPosition, 1, cubeYPosition);
                var cubePrefab = ObjectManager.Instance.Spawn("Cube", pos, Quaternion.identity, cubeHolder);
                var meshRenderer = cubePrefab.GetComponent<MeshRenderer>();
                meshRenderer.material.color = color;

                GameManager.Instance.TotalSpawnedObjectCount++;
            }
        }
    }

}


