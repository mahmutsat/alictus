﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public int TotalSpawnedObjectCount { get; set; } = 0;
    public int TotalCollectedObjectCount
    {
        get
        {
            return totalcollectedObjectsCount;
        }

        set
        {
            totalcollectedObjectsCount = value;

            UIManager.Instance.collectedObjectsBar.fillAmount = totalcollectedObjectsCount / (float)TotalSpawnedObjectCount;
        }
    }

    int totalcollectedObjectsCount = 0;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
    }
}
