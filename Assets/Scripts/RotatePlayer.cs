﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class RotatePlayer : MonoBehaviour, IDragHandler
{
    [Space]
    [SerializeField]
    Transform player;
    [SerializeField]
    float dragSensitivity = 100f;

    public void OnDrag(PointerEventData eventData)
    {
        float dragX = Input.GetAxis("Mouse X") * dragSensitivity;
        player.Rotate(Vector3.up, dragX * Time.deltaTime);
    }
}
